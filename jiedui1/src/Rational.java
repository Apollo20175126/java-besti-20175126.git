public class Rational {
   double numerator = 1 ;   //分子
   double denominator = 1; //分母
   void setNumerator(double a) {  //设置分子
      double c=f(Math.abs(a),denominator);  //计算最大公约数
      numerator = a/c;//约分
      denominator = denominator/c;
      if(numerator<0&&denominator<0) { //分子分母负数情况
          numerator = -numerator;
          denominator = -denominator;
      }
   }
   void setDenominator(double b) {  //设置分母
      double c=f(numerator,Math.abs(b));  //计算最大公约数
      numerator = numerator/c;
      denominator = b/c;
      if(numerator<0&&denominator<0) {
          numerator = -numerator;
          denominator = -denominator;
      }
   }
   double getNumerator() {//返回分子
      return numerator;
   }
   double getDenominator() {
     return denominator;
   }  
   double f(double a,double b) { //求a和b的最大公约数
      if(a==0) return 1;
      if(a<b) {
         double c=a;
         a=b;
         b=c; 
      }
      double r=a%b;
      while(r!=0) { //辗转相除
         a=b;
         b=r;
         r=a%b;
      } 
      return b;
   }
   Rational add(Rational r) {  //加法运算
      double a=r.getNumerator();
      double b=r.getDenominator();
      double newNumerator=numerator*b+denominator*a; //得结果的分子
      double newDenominator=denominator*b;           //得结果的分母
      Rational result=new Rational(); 
      result.setNumerator(newNumerator);//返回答案分子
      result.setDenominator(newDenominator);//返回答案分母
      return result;
   }
   Rational sub(Rational r) {  //减法运算
      double a=r.getNumerator();
      double b=r.getDenominator();
      double newNumerator=numerator*b-denominator*a;
      double newDenominator=denominator*b;
      Rational result=new Rational(); 
      result.setNumerator(newNumerator);
      result.setDenominator(newDenominator);
      return result;
   }
   Rational muti(Rational r) { //乘法运算
      double a=r.getNumerator();
      double b=r.getDenominator();
      double newNumerator=numerator*a;
      double newDenominator=denominator*b;
      Rational result=new Rational(); 
      result.setNumerator(newNumerator);
      result.setDenominator(newDenominator);
      return result;
   }
   Rational div(Rational r)  { //除法运算
      double a=r.getNumerator();
      double b=r.getDenominator();
      double newNumerator=numerator*b;
      double newDenominator=denominator*a;
      Rational result=new Rational(); 
      result.setNumerator(newNumerator);
      result.setDenominator(newDenominator);
      return result;
   }
   static boolean judge(Rational a,Rational b) {//判断是否正确
	   if(a.numerator==b.numerator && a.denominator==b.denominator)
		   return true;
	   else
		   return false;   
   }
}
