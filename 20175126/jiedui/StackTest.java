import java.lang.*;
import java.util.*;
import java.util.Scanner;

public class StackTest {
    
    
    static class mystack
    {
        int mytop;
        int stack[];
        
        public mystack(int num) {
            mytop=-1;
            stack=new int[num];
        }
        /*出栈*/
        void mypop()
        {
            mytop--;
        }
        /*入栈*/
        void mypush(int x)
        {
            mytop++;
            stack[mytop]=x;
            
        }
        /*判空*/
        Boolean myisempty()
        {
            if(mytop==-1)
                return true;
            else
                return false;
        }
        /*取栈顶元素*/
        int mypeek()
        {
            int peek=stack[mytop];
            return peek;
        }
        /*栈大小*/
        int mysize()
        {
            return mytop+1;
        }
    }
    
    
    public static void main(String[] args) {
        mystack myStack=new mystack(20);
	for(int i = 0;i<=10;i++){
		int m = (int)(Math.random()*(10)+1);
        myStack.mypush(m);
	}
        System.out.print("栈大小为"+myStack.mysize());
        System.out.println();
        for(int i=myStack.mytop;i>=0;i--)
        {
            int get=myStack.mypeek();
            myStack.mypop();
            System.out.println(get);
        }
        
    }
    
    

}
